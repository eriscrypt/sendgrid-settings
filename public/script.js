window.addEventListener("DOMContentLoaded", () => {
  console.log("loaded page");

  const form = document.getElementById("form");

  form.addEventListener("submit", (e) => {
    e.preventDefault();

    const emailInput = document.getElementById("email");
    const email = emailInput.value;

    fetch(`/addMember/${email}`, {
      method: "POST",
    })
      .then((res) => res.json())
      .then((res) => {
        console.log(res);
      })
      .catch((err) => {
        console.log(err);
      });
  });
});
