require("dotenv").config();

const path = require("path");
const express = require("express");
const request = require("request");
const sgMail = require("@sendgrid/mail");

const PORT = process.env.PORT || 5001;
const SENDGRID_KEY = process.env.SENDGRID_KEY;

sgMail.setApiKey(SENDGRID_KEY);

const app = express();

app.use(express.static(path.join(__dirname, "public")));
app.get("*", (req, res) => {
  return res.sendFile(path.join(__dirname, "public", "index.html"));
});

// Get list of contact list
const getLists = () => {
  const options = {
    method: "GET",
    url: "https://api.sendgrid.com/v3/marketing/lists",
    headers: {
      authorization: `Bearer ${SENDGRID_KEY}`,
    },
  };

  request(options, (err, response) => {
    if (err) {
      console.log(err);
    }

    const { result } = JSON.parse(response.body);

    // Here your all lists
    // Your need copy the 'id' of current list and paste on 'list_ids'
    console.log(result);
  });
};

getLists();

app.post("/addMember/:email", (req, res) => {
  const { email } = req.params;

  const msg = {
    to: email, // Change to your recipient
    from: "eriscrypt@gmail.com", // Change to your verified sender
    subject: "You are enrolled",
    html: "<h1>Test message</h1>", // Your message
  };

  sgMail
    .send(msg)
    .then((response) => {
      var options = {
        method: "PUT",
        url: "https://api.sendgrid.com/v3/marketing/contacts",
        headers: {
          authorization: `Bearer ${SENDGRID_KEY}`,
        },
        body: JSON.stringify({
          // HERE PAST THE ID OF LIST FROM 'getLists' function upper
          list_ids: ["8ecc1b6a-2e90-421d-899e-e02c47655327"],
          contacts: [{ email: email }],
        }),
      };

      request(options, (err, response) => {});

      return res.status(200).json({ success: true });
    })
    .catch((error) => {
      console.log(error);
      return res.status(200).json({ success: false });
    });
});

app.listen(PORT, () => {
  console.log(`server running on port ${PORT}`);
});
